package com.example.EmployeeDatabase.component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Scanner;

@Entity
@Table(name="Employee2")
public class Employee2 {
    @Id
    @Column(name="Eid")
    private int Eid;
    @Column(name="EmpName")
    private String EmpName;
    private int salary;
    private String depart;

    public Employee2() {
    }

    public Employee2(int eid, String empName, int salary, String depart) {
        Eid = eid;
        EmpName = empName;
        this.salary = salary;
        this.depart = depart;
    }

    public int getEid() {
        return Eid;
    }

    public void setEid(int eid) {
        Eid = eid;
    }

    public String getEmpName() {
        return EmpName;
    }

    public void setEmpName(String empName) {
        EmpName = empName;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void initialize(){
        Scanner scanner=new Scanner(System.in);
        Eid=scanner.nextInt();
        EmpName=scanner.next();
        salary=scanner.nextInt();
        depart=scanner.next();
    }
}
