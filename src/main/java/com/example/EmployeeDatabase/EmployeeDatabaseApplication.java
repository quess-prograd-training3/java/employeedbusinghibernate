package com.example.EmployeeDatabase;

import com.example.EmployeeDatabase.component.Employee2;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class EmployeeDatabaseApplication {

	public static void main(String[] args) {

		//SpringApplication.run(EmployeeDatabaseApplication.class, args);
		String ConfigurationFile="hibernate.cfg.xml";
		ClassLoader classLoaderObj=EmployeeDatabaseApplication.class.getClassLoader();
		File file=new File(classLoaderObj.getResource(ConfigurationFile).getFile());
		SessionFactory sessionFactoryObj=new Configuration().configure().buildSessionFactory();
		Session sessionObject=sessionFactoryObj.openSession();
		Scanner scanner = new Scanner(System.in);
		System.out.println("enter the operation you want to perform");
		System.out.println("Enter 1 for save records");
		System.out.println("Enter 2 for update records");
		System.out.println("Enter 3 for delete records");
		System.out.println("Enter 4 for display records");
		int input = scanner.nextInt();
		if (input ==1){
			saveRecord(sessionObject);
		} else if (input==2) {
			update(sessionObject);
		} else if (input==3) {
			delete(sessionObject);
		} else if (input==4) {
			display(sessionObject);
		}
		else {
			System.out.println("Enter a valid input");
		}
	}
	public static void saveRecord(Session sessionObj){
		Employee2 employee=new Employee2();
		employee.initialize();
		sessionObj.beginTransaction();
		sessionObj.save(employee);
		sessionObj.getTransaction().commit();
		System.out.println("Record Added Successfully");
	}

	public static void display(Session sessionObj){
		Query queryObj=sessionObj.createQuery("FROM Employee2");
		List<Employee2> listObj=queryObj.list();
		for(Employee2 iterate:listObj){
			System.out.println(iterate.getEid()+" "+iterate.getEmpName()+" "+iterate.getSalary()+" "+iterate.getDepart());
		}
	}

	public static void update(Session sessionObj){
		int Eid=1;
		Employee2 employee2Obj=(Employee2) sessionObj.get(Employee2.class,Eid);
		employee2Obj.setEmpName("MohanSai");
		sessionObj.beginTransaction();
		sessionObj.saveOrUpdate(employee2Obj);
		sessionObj.getTransaction().commit();
		System.out.println("Update Record");
	}

	public static void delete(Session sessionObj){
		int Eid=6;
		Employee2 employee2Obj=(Employee2) sessionObj.get(Employee2.class,Eid);
		sessionObj.beginTransaction();
		sessionObj.delete(employee2Obj);
		sessionObj.getTransaction().commit();
		System.out.println("delete Record");
	}
}
